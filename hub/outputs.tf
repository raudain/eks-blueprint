
output "configure_kubectl" {
  description = "Configure kubectl: make sure you're logged in with the correct AWS profile and run the following command to update your kubeconfig"
  value       = <<-EOT
    aws eks --region ${local.region} update-kubeconfig --name ${module.eks.cluster_name} --alias hub
  EOT
}

output "cluster_name" {
  description = "Cluster Hub name"
  value       = module.eks.cluster_name
}
output "cluster_endpoint" {
  description = "Cluster Hub endpoint"
  value       = module.eks.cluster_endpoint
}
output "cluster_certificate_authority_data" {
  description = "Cluster Hub certificate_authority_data"
  value       = module.eks.cluster_certificate_authority_data
}
output "cluster_region" {
  description = "Cluster Hub region"
  value       = local.region
}
output "hub_node_security_group_id" {
  description = "Cluster Hub region"
  value       = module.eks.node_security_group_id
}


output "gitops_addons_url" {
  value = local.gitops_addons_url
}

output "gitops_addons_path" {
  value = local.gitops_addons_path
}

output "gitops_addons_revision" {
  value = local.gitops_addons_revision
}
    
output "gitops_addons_basepath" {
  value = local.gitops_addons_basepath
} 

output "gitops_platform_url" {
  value = local.gitops_platform_url
}

output "gitops_platform_path" {
  value = local.gitops_platform_path
}

output "gitops_platform_revision" {
  value = local.gitops_platform_revision
}
    
output "gitops_platform_basepath" {
  value = local.gitops_platform_basepath
} 

output "gitops_workload_url" {
  value = local.gitops_workload_url
}

output "gitops_workload_path" {
  value = local.gitops_workload_path
}

output "gitops_workload_revision" {
  value = local.gitops_workload_revision
}
    
output "gitops_workload_basepath" {
  value = local.gitops_workload_basepath
} 

