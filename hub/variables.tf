variable "vpc_id" {
  description = "vps id"
  type = string
  default = "vpc-029365cec29430488"
  
}
variable "kubernetes_version" {
  description = "EKS version"
  type        = string
  default     = "1.28"
}

variable "eks_admin_role_name" {
  description = "EKS admin role"
  type        = string
  default     = "eks-access"
}

variable "addons" {
  description = "EKS addons"
  type        = any
  default = {
    enable_aws_load_balancer_controller = true
    enable_aws_argocd = true
  }
}

variable "authentication_mode" {
  description = "The authentication mode for the cluster. Valid values are CONFIG_MAP, API or API_AND_CONFIG_MAP"
  type        = string
  default     = "API_AND_CONFIG_MAP"
}

variable "gitops_addons_url" {
  type        = string
  description = "Git repository addons url"
  default     = "https://github.com/reneaudain/eks-blueprints-for-terraform-workshop.git"
}
variable "gitops_platform_url" {
  type        = string
  description = "Git repository platform url"
  default     = "https://github.com/reneaudain/eks-blueprints-for-terraform-workshop.git"
}
variable "gitops_workload_url" {
  type        = string
  description = "Git repository platform url"
  default     = "https://github.com/reneaudain/eks-blueprints-for-terraform-workshop.git"
}
variable "gitops_addons_basepath" {
  type        = string  
  description = "Git repository base path for addons"
  default     = "assets/platform/addons/"
}
variable "gitops_addons_path" {
  type        = string  
  description = "Git repository path for addons"
  default     = "applicationset/"
}
variable "gitops_addons_revision" {
  type        = string  
  description = "Git repository revision/branch/ref for addons"
  default     = "HEAD"
}
variable "gitops_platform_basepath" {
  type        = string  
  description = "Git repository base path for platform"
  default     = "assets/platform/"
}
variable "gitops_platform_path" {
  type        = string  
  description = "Git repository path for platform"
  default     = "bootstrap"
}
variable "gitops_platform_revision" {
  type        = string  
  description = "Git repository revision/branch/ref for platform"
  default     = "HEAD"
}
variable "gitops_workload_basepath" {
  type        = string  
  description = "Git repository base path for platform"
  default     = "assets/developer/"
}
variable "gitops_workload_path" {
  type        = string  
  description = "Git repository path for workload"
  default     = "gitops/apps"
}
variable "gitops_workload_revision" {
  type        = string  
  description = "Git repository revision/branch/ref for platform"
  default     = "HEAD"
}
variable "gitops_istio_basepath" {
  type        = string  
  description = "Git repository base path for istio"
  default     = "assets/istio/"
}
variable "gitops_istio_path" {
  type        = string  
  description = "Git repository path for istio"
  default     = "gitops/apps"
}
variable "gitops_istio_revision" {
  type        = string  
  description = "Git repository revision/branch/ref for istio"
  default     = "HEAD"
}
variable "gitops_istio_url" {
  type        = string
  description = "Git repository istio url"
  default     = "https://github.com/reneaudain/eks-blueprints-for-terraform-workshop.git"
}


